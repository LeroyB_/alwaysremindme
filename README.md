# AlwaysRemindMe

iOS Tweak to always show a label on Homescreen, Lockscreen, both, or above everthing else. Configured form setting.app. Tested on iOS 10 and 11. Please report bugs to me, preferably via an issue here:
https://gitlab.com/LeroyB_/alwaysremindme/issues/new

All my project are open sourced and are meant to be for learning.
Please view the license file for more info.
